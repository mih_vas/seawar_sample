unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids;

type
  TForm1 = class(TForm)
    GamerStringGrid: TStringGrid;
    EnemyStringGrid: TStringGrid;
    Button1: TButton;
    procedure GamerStringGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure EnemyStringGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure EnemyStringGridSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  TMap = array [0..11, 0..11] of Integer;
  TWinner = (Nobody, Gamer, Enemy);

var
  Form1: TForm1;
  EnemyField, GamerField: TMap;
  isGame: Boolean;
  WhoTurn: TWinner;

implementation

{$R *.dfm}

function InitMap(var AMap: TMap): Boolean;
var
  i, j, k, x, y, num: Byte;
  ori, peresecheniy_net: Boolean;
begin
  Result := False;
  //��������� ��������
  k := 1;
  Repeat
   //���������� �����
   Case k Of
   1     : num := 4;
   2,3   : num := 3;
   4..6  : num := 2;
   7..10 : num := 1;
   End;
   //���������� �������
   ori := Random > 0.5;
   //���������� �������
   If ori Then Begin
     x := Random(10-num+1) + 1;
     y := Random(10) + 1;
   End
   Else Begin
     x := Random(10) + 1;
     y := Random(10-num+1) + 1
   End;
   //�������� �� ����������� � ������� ���������
   peresecheniy_net := True;
   If ori Then Begin
     For i:=0 To num-1 Do
       If AMap[x+i,y] <> 0 Then peresecheniy_net := False;
   End
   Else Begin
     For i:=0 To num-1 Do
       If AMap[x,y+i] <> 0 Then peresecheniy_net := False;
   End;
   //���� ��� ����������� � ������� ���������
   If peresecheniy_net Then Begin
     //��������� MAP ���������� ������� �������
     If ori Then Begin
       For i:=0 To num-1 Do
         AMap[x+i,y] := k;
     End
     Else Begin
       For i:=0 To num-1 Do
         AMap[x,y+i] := k;
     End;
     //�������� ������� -1
     If ori Then Begin
       For i:=0 To num-1 Do Begin
         If i = 0 Then Begin
           AMap[x-1,y] := -1;
           AMap[x-1,y-1] := -1;
           AMap[x-1,y+1] := -1;
         End;
         AMap[x+i,y-1] := -1;
         AMap[x+i,y+1] := -1;
         If i = num-1 Then Begin
           AMap[x+num,y] := -1;
           AMap[x+num,y-1] := -1;
           AMap[x+num,y+1] := -1;
         End;
       End;
     End
     Else Begin
       For i:=0 To num-1 Do Begin
         If i = 0 Then Begin
           AMap[x,y-1] := -1;
           AMap[x-1,y-1] := -1;
           AMap[x+1,y-1] := -1;
         End;
         AMap[x-1,y+i] := -1;
         AMap[x+1,y+i] := -1;
         If i = num-1 Then Begin
           AMap[x,y+num] := -1;
           AMap[x-1,y+num] := -1;
           AMap[x+1,y+num] := -1;
         End;
       End
     End;
     k := k + 1;
   End;
  Until k > 10;
  Result := True;
end;

function AI(var AMap: TMap): Boolean;
var
  Shot: TPoint;
begin
  Result := False;
  repeat
    Shot.X := Random(10) + 1;
    Shot.Y := Random(10) + 1;
  until (AMap[Shot.X, Shot.Y] <> -2) or (AMap[Shot.X, Shot.Y] <> -3);
  case AMap[Shot.X, Shot.Y] of
  -3:    begin
           AMap[Shot.X, Shot.Y] := -3;
         end;
  -2..0: begin
           AMap[Shot.X, Shot.Y] := -2;
         end;
  1..10: begin
           AMap[Shot.X, Shot.Y] := -3;
           Result := True;
         end;
  end;
end;

function CheckEndGame: Boolean;
  function CheckWin: TWinner;
  var
    i, j: Integer;
    Flag: Boolean;
    function CheckShip(AMap: TMap): Boolean;
    var
      i, j: Integer;
    begin
      Result := True;
      for i := 1 to 10 do begin
        for j := 1 to 10 do begin
          if AMap[i, j] > 0 then begin
            Result := False;
            exit;
          end;
        end;
      end;
    end;
  begin
    if CheckShip(EnemyField) then begin
      Result := Gamer;
      exit;
    end;
    if CheckShip(GamerField) then begin
      Result := Enemy;
      exit;
    end;
    Result := Nobody;
  end;
begin
  case CheckWin of
  Nobody: begin
            Result := False;
          end;
  Gamer:  begin
            ShowMessage('����� �������');
            isGame := False;
            Result := True;
          end;
  Enemy:  begin
            ShowMessage('��������� �������');
            isGame := False;
            Result := True;
          end;
  end;
end;

procedure ShotComputer;
begin
  if WhoTurn = Enemy then begin
    while AI(GamerField) do begin
      Form1.GamerStringGrid.Repaint;
      if CheckEndGame then exit;
    end;
    Form1.GamerStringGrid.Repaint;
    WhoTurn := Gamer;
  end;
end;

procedure TForm1.GamerStringGridDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  case GamerField[ACol+1, ARow+1] of
  -3:    begin
           GamerStringGrid.Canvas.Brush.Color := clGray;
         end;
  -2:    begin
           GamerStringGrid.Canvas.Brush.Color := clBlack;
         end;
  -1:    begin
           GamerStringGrid.Canvas.Brush.Color := clBlue;
         end;
  0:     begin
           GamerStringGrid.Canvas.Brush.Color := clBlue;
         end;
  1..10: begin
           GamerStringGrid.Canvas.Brush.Color := clGreen;
         end;
  end;
  GamerStringGrid.Canvas.Rectangle(Rect);
end;

procedure TForm1.EnemyStringGridDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  case EnemyField[ACol+1, ARow+1] of
  -3:    begin
           EnemyStringGrid.Canvas.Brush.Color := clGray;
         end;
  -2:    begin
           EnemyStringGrid.Canvas.Brush.Color := clBlack;
         end;
  -1..10: begin
            EnemyStringGrid.Canvas.Brush.Color := clBlue;
          end;
  end;
  EnemyStringGrid.Canvas.Rectangle(Rect);
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  i, j: Byte;
begin
  WhoTurn := Nobody;
  isGame := False;
  for i := 0 to 11 do begin
    for j := 0 to 11 do begin
      EnemyField[i, j] := 0;
      GamerField[i, j] := 0;
    end;
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  i, j: Byte;
begin
  isGame := InitMap(GamerField) and InitMap(EnemyField);
  if not isGame then begin
    ShowMessage('���� �� ������������. ���������� ��� ���.');
    exit;
  end;
  if Random > 0.5 then WhoTurn := Gamer else WhoTurn := Enemy;
  GamerStringGrid.Repaint;
  EnemyStringGrid.Repaint;
  if WhoTurn = Enemy then begin
    ShotComputer;
  end;
end;

procedure TForm1.EnemyStringGridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if (isGame) and (WhoTurn = Gamer) then begin
    case EnemyField[ACol+1, ARow+1] of
    -3: begin
             EnemyField[ACol+1, ARow+1] := -3;
             WhoTurn := Enemy;
             EnemyStringGrid.Repaint;
             ShotComputer;
           end;
    -2..0: begin
             EnemyField[ACol+1, ARow+1] := -2;
             WhoTurn := Enemy;
             EnemyStringGrid.Repaint;
             ShotComputer;
           end;
    1..10: begin
             EnemyField[ACol+1, ARow+1] := -3;
             EnemyStringGrid.Repaint;
           end;
    end;
    if CheckEndGame then exit;
  end;
end;

end.
