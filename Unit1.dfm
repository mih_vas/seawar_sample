object Form1: TForm1
  Left = 192
  Top = 122
  Width = 568
  Height = 266
  Caption = #1052#1086#1088#1089#1082#1086#1081' '#1073#1086#1081
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GamerStringGrid: TStringGrid
    Left = 8
    Top = 8
    Width = 209
    Height = 209
    BorderStyle = bsNone
    ColCount = 10
    DefaultColWidth = 20
    DefaultRowHeight = 20
    FixedCols = 0
    RowCount = 10
    FixedRows = 0
    TabOrder = 0
    OnDrawCell = GamerStringGridDrawCell
  end
  object EnemyStringGrid: TStringGrid
    Left = 320
    Top = 8
    Width = 209
    Height = 209
    BorderStyle = bsNone
    ColCount = 10
    DefaultColWidth = 20
    DefaultRowHeight = 20
    FixedCols = 0
    RowCount = 10
    FixedRows = 0
    TabOrder = 1
    OnDrawCell = EnemyStringGridDrawCell
    OnSelectCell = EnemyStringGridSelectCell
  end
  object Button1: TButton
    Left = 232
    Top = 88
    Width = 75
    Height = 25
    Caption = #1053#1086#1074#1072#1103
    TabOrder = 2
    OnClick = Button1Click
  end
end
